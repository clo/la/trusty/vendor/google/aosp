#!/bin/sh
"." "`dirname $0`/envsetup.sh"
"exec" "$PY3" "$0" "$@"
#
# Copyright (C) 2022 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Generate from an aidl-generated interface header, its client c wrapper."""

from pathlib import Path
import argparse
import subprocess
import glob
from c_wrapper import iface_to_c

script_dir = Path(__file__).parent
root_dir = script_dir / "../../../../../.."


def run_aidl(aidl_file, aidl_tool_str: str, out_cpp: str, out_hdr: str):
    aidl_tool = Path(aidl_tool_str)
    if not aidl_tool.exists():
        raise (
            "aidl tool cannot be found at {}, please build qemu-generic-64 target\n".format(
                aidl_tool.resolve()
            )
        )
    aidl_path = Path(aidl_file)
    completed_process = subprocess.run(
        " ".join(
            [
                aidl_tool.as_posix(),
                "--lang=trusty",
                "-h",
                out_hdr,
                "-o",
                out_cpp,
                aidl_path.as_posix(),
            ]
        ),
        shell=True,
        text=True,
        capture_output=True,
    )
    if completed_process.returncode:
        raise Exception(completed_process.stderr)
    return Path(out_hdr) / "{}.h".format(aidl_path.stem)


def run_clang_format(d):
    files = []
    for f in glob.glob("{}/*.cpp".format(d)):
        files.append(f)
    for f in glob.glob("{}/*.h".format(d)):
        files.append(f)
    for f in files:
        completed_process = subprocess.run(
            " ".join(
                [
                    "clang-format",
                    "-i",
                    f,
                ]
            ),
            shell=True,
            text=True,
            capture_output=True,
        )
        if completed_process.returncode:
            raise Exception(completed_process.stderr)


def main(*cmd_line):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INPUT",
        type=str,
        help="An AIDL-generated interface header file or an AIDL file.",
    )
    parser.add_argument(
        "--domain",
        type=str,
        choices=["ql_tipc", "trusty_user"],
        help="c client domain.",
    )
    parser.add_argument(
        "--bn",
        action="store_true",
        help="Bn (server) mode - do not generate Bp C wrapper.",
    )
    parser.add_argument(
        "--append-cpp",
        action="store_true",
        help="append generated C code (Bp C wrapper) into the interface's cpp.",
    )
    parser.add_argument(
        "--dbg",
        action="store_true",
        help="debug mode - stores ast and methods json files.",
    )
    parser.add_argument(
        "--out", type=str, help="base output directory for generated files"
    )
    parser.add_argument(
        "--header",
        type=str,
        help="header output directory for the generated header files.",
    )
    parser.add_argument(
        "--aidl-tool",
        type=str,
        help="aidl tool path.",
    )
    if len(cmd_line) > 0:
        args = parser.parse_args(cmd_line)
    else:
        args = parser.parse_args()
    file_input = Path(args.INPUT)
    iface_name = file_input.stem
    if file_input.suffix == ".h":
        iface_hdr = Path(args.header) / "{}.h".format(iface_name)
    elif file_input.suffix == ".aidl":
        iface_hdr = run_aidl(
            file_input.as_posix(), args.aidl_tool, args.out, args.header
        )
    else:
        raise Exception(
            "input file `{}` not supported header, please specify with a .h or .aidl file".format(
                file_input.as_posix()
            )
        )
    if not iface_hdr.exists():
        raise Exception(
            "interface header `{}` not found, please run aidl tool".format(
                iface_hdr.as_posix()
            )
        )
    iface_to_c(
        iface_hdr,
        args.domain,
        args.out,
        args.header,
        bn=args.bn,
        append_cpp=args.append_cpp,
        dbg=args.dbg,
    )
    for d in [args.out, args.header]:
        run_clang_format(d)


def example():
    # example command lines for generating trusty_user and ql_tipc flavors
    for aidl_file in [
        root_dir / "trusty/user/base/interface/boot_done/IBootDone.aidl",
    ]:
        aidl_dir = aidl_file.parent
        for domain in ["trusty_user", "ql_tipc"]:
            generated = "generated"
            if domain == "ql_tipc":
                generated += "_ql_tipc"
            main(
                "--domain",
                domain,
                "--out",
                (aidl_dir / generated).as_posix(),
                "--header",
                (aidl_dir / generated / "include").as_posix(),
                "--aidl-tool",
                (root_dir / "trusty/prebuilts/aosp/tidl/tidl").as_posix(),
                aidl_file.as_posix(),
            )


if __name__ == "__main__":
    main()
